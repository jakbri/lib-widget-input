

(function () {
    // Add short cut to various toronto functions
    if (typeof D === "undefined"){
        T = Toronto.findDataset;
        W = Toronto.findWidget;
        D = Toronto.getDatasetFieldValue;
    }

    // Add better error message for when there is no current row.
    Toronto.framework.StateUtils.getDatasetFieldValue = function(state, datasetField, defaultValue) {
        var fieldInfo = this._parseDatasetField(datasetField);
        var currentRow = this._getCurrentDatasetRow(state, fieldInfo.datasetName);
        if (currentRow === null && !(YAHOO.lang.isUndefined(defaultValue))) {
            return defaultValue;
        }

        if (currentRow === null){
            throw Error('Unable to get dataset field value for {$' + datasetField +'} as there is no current row.');
        }

        var value = currentRow.getField(fieldInfo.fieldName);
        if (value === null && !(YAHOO.lang.isUndefined(defaultValue))) {
            return defaultValue;
        }
        return value;
    };

    YAHOO.namespace("com.aviarc.form.v1_0_0");
    var FORM = YAHOO.com.aviarc.form.v1_0_0;

    var L = YAHOO.lang;
    var D  = YAHOO.util.Dom;

    FORM.DefaultFormWidget = function(){};

    L.extend(FORM.DefaultFormWidget, Toronto.framework.DefaultWidgetImpl, {

        startup: function(widgetContext) {
            FORM.DefaultFormWidget.superclass.startup.apply(this, arguments);

            this._isRendered = false;
            this._isEditable = true;

            this._label = this.getAttribute('label');
            this._editable = this.getAttribute('editable');
            this._showInvalid = parseBoolean(this.getAttribute('show-invalid'));
            this._container = this.getNamedElement('container');

            var classes = this.getAttribute('class');
            
            if (classes !== ''){
                D.addClass(this._container, classes);
            }

            // editable is based off a field, rather than a set true/false
            if (this._editable.indexOf('.') > -1 ){
                var split = this._editable.split('.');
                this._editable = { 'dataset': split[0], 'field': split[1] };
            } else {
                this._isEditable = parseBoolean(this._editable);
                this._editable = null;
                this._editFieldChanged();
            }

            this._metadataEvents = [];
            this._metadataField = null;
        },

        bindMetadata: function(fieldObject){
            if (this._showInvalid === false){
                return;
            }

            // Same object do nothing
            if (fieldObject === this._metadataField){
                return;
            }

            // Unbind the old events
            for (var i = 0; i < this._metadataEvents.length; i++) {
                this._metadataEvents[i].unbind();
            }
            this._metadataEvents = [];

            var metadata = fieldObject.getMetadata();
            metadata.onValidChanged.bindHandler(this._onValidChange, this, this.toString() + ':valid');
            // metadata.onReadOnlyChanged.bindHandler(this._onReadOnlyChange, this, this.toString() + ':valid');
            // metadata.onMandatoryChanged.bindHandler(this._onMandatoryChange, this, this.toString() + ':valid');
        },

        bind: function(dataContext) {
            FORM.DefaultFormWidget.superclass.bind.apply(this, arguments);

            if (this._editable !== null){
                this._editable.datasetRef = dataContext.findDataset(this._editable.dataset);
                var editEvents = this._editable.datasetRef.getEvents();

                editEvents.getEventByName('onCurrentRowFieldChanged', this._editable.field).bindHandler( this._editFieldChanged, this, this.toString() + ':editable');
                editEvents.getEventByName('onContentsReplaced').bindHandler( this._editFieldChanged, this, this.toString() + ':editable');
                editEvents.getEventByName('onCurrentRowChanged').bindHandler( this._editFieldChanged, this, this.toString() + ':editable');

                this._editFieldChanged();
            }
        },

        refresh: function() {
            FORM.DefaultFormWidget.superclass.refresh.apply(this, arguments);
            // console.log(this + ': refresh', this._isRendered);

            if (this._isRendered === false){
                this.render();
                this._isRendered = true;
            }

            if (this._labelElement !== null){
                this._labelElement.innerHTML = this._label + ':';
            }

            if (this._showInvalid){
                this._onValidChange();
            }
        },

        createElement: function(htmlType, elementName){
            var element = document.createElement(htmlType);
            if (elementName !== undefined){
                element.setAttribute('id', this.getWidgetID() + ':' + elementName);
            }
            return element;
        },

        render: function(){

            if (this._label === undefined || this._label === ''){
                this._labelElement = null;
                D.addClass(this._container, 'no-label');

            } else {
                this._labelElement = this.createElement('label', 'label');
                this._labelElement.setAttribute('class', 'label');

                this._container.appendChild(this._labelElement);
            }

            if (this._showInvalid){
                this._invalidElement = this.createElement('span', 'invalid-text');
                this._invalidElement.setAttribute('class', 'invalid-text');

                this._container.appendChild(this._invalidElement);
            }
        },

        _editFieldChanged: function(o){
            var oldValue = this._isEditable;
            var currentRow;

            if (this._editable !== null){
                currentRow = this._editable.datasetRef.getCurrentRow();
                if (currentRow === null){
                    //console.log (this + ': Unable to check editablity, as the editable dataset "' + this._editable.dataset + '" has no currentRow');
                    return;
                }

                var editField = currentRow.getField(this._editable.field);
                this._isEditable = parseBoolean(editField);
            }

            this._toggleEditableClasses(this._isEditable);

            if (this._isRendered !== false){
                if (oldValue !== this._isEditable){
                    //console.log(this + ': Editablity changed ' + this._isEditable);
                    if (typeof this._onEditChange == "function"){
                        this._onEditChange();
                    }
                }
            }

        },

        setEditable: function(editable) {
            this._isEditable = parseBoolean(editable)
            this._toggleEditableClasses(this._isEditable);
        },

        setVisible: function(visible){
            if (visible !== true && visible !== false){
                visible = parseBoolean(visible);
            }

            if (visible === false){
                D.addClass(this._container, 'display-none');
            } else {
                D.removeClass(this._container, 'display-none');
            }
        },

        getDisplayName: function(){
            if (this._label !== ''){
                return this._label;
            }

            return this.getWidgetID();
        },

        validate: function(){

            var row = this._dataset.getCurrentRow();
            if (row !== null){
                var field = row.getFieldObject(this._field.field);
                var metadata = field.getMetadata();

                // Invalid
                if (metadata.isValid() === false){
                    return metadata.getValidationInfo();
                }

                // check Mandatory
                // if (metadata.isMandatory() === true){
                //     var value = field.getValue();
                //     if (value === '' || value === null){
                //         return [new Toronto.core.WidgetValidationInfo('You must enter a value in this field',
                //                                             this.getWidgetID())];
                //     }
                // }
            }

            return true;
        },

        _onValidChange: function(o){

            var row = this._dataset.getCurrentRow();
            var reason = "";
            if (row === null){
                D.removeClass(this._container, "invalid");
                reason = "";
            } else {
                var metadata = row.getFieldObject(this._field.field).getMetadata();
                valid = metadata.isValid();
                if (valid){
                    D.removeClass(this._container, "invalid");
                } else {
                    D.addClass(this._container, "invalid");
                    reason = metadata.getInvalidReason().join('\n');
                }
            }

            if (this._showInvalid){
                if (reason === null || reason === undefined){
                    reason = "";
                }
                this._invalidElement.innerHTML = reason;
            }

        },

        _toggleEditableClasses: function(editable){
            if (editable === true){
                D.addClass(this._container, 'editable');
                D.removeClass(this._container, 'uneditable');
            } else {
                D.removeClass(this._container, 'editable');
                D.addClass(this._container, 'uneditable');
            }
        },

        toString: function(){
            return 'DefaultFormWidget';
        },

        isEditable: function(){
            return this._isEditable;
        },

        getCssPrefix: function(){
            var cssPrefix = 'com-aviarc-form_1-0-0_';
            if (this.getNodeName === undefined){
                return cssPrefix + 'form';
            }
            return cssPrefix + this.getNodeName();
        },

        createRenderedNode: function(context) {

            var widget = new this.constructor();
            widget.setXMLContext(context.xmlContext);

            var containerDiv = document.createElement("div");

            var prefix = this.getCssPrefix();
            var classString = context.xmlContext.getAttribute("class");

            var classList = classString ? classString.toLowerCase().split(" ") : [];
            containerDiv.className = Toronto.util.CssUtils.makeClassString(prefix, classList, []);

            containerDiv.id = context.xmlContext.getAttribute("name") + ":container";

            var attributes = context.xmlContext.getAttributes();
            Toronto.util.Positioning.setContainerPositionStyles(containerDiv, attributes);

            // Need to refactor this child stuff so anybody can do it.
            var childRenderedNodes = [];

            childRenderedNodes = Toronto.util.ClientRendering.renderChildren(context, containerDiv);

            return {

                getDOMNode: function() { return containerDiv; },

                getWidget: function() { return widget; },

                getChildren: function() { return childRenderedNodes; }

            };

        }

    });
})();
