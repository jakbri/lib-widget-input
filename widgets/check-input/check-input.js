
(function () {

    YAHOO.namespace("lib_widget_input");
    var FORM = YAHOO.lib_widget_input;

    var L = YAHOO.lang;
    var D  = YAHOO.util.Dom;

    FORM.CheckInput = function(){};

    L.extend(FORM.CheckInput, FORM.DefaultFormWidget, {

        startup: function(widgetContext) {
            FORM.CheckInput.superclass.startup.apply(this, arguments);

            var field = this.getAttribute('field').split('.');
            this._field = {'dataset': field[0], 'field': field[1]};

            this._checkedValue = this.getAttribute('checked-value');
            this._uncheckedValue = this.getAttribute('unchecked-value');
            this._defaultValue = this.getAttribute('default');
        },

        bind: function(dataContext) {
            FORM.CheckInput.superclass.bind.apply(this, arguments);

            this._dataset = dataContext.findDataset(this._field.dataset);

            var events = this._dataset.getEvents();
            this._dataset.bindOnCurrentRowFieldChangedHandler(this._field.field, this._updateInput, this);
            this._dataset.onContentsReplaced.bindHandler( this._updateInput, this);
            this._dataset.onCurrentRowChanged.bindHandler( this._updateInput, this);
        },

        refresh: function() {
            FORM.CheckInput.superclass.refresh.apply(this, arguments);

            this._updateInput();
        },

        _updateInput: function(){
            if (this._isRendered === false){
                return;
            }

            var currentRow = this._dataset.getCurrentRow();
            var value = "";
            if (currentRow !== null){
                value = currentRow.getFieldObject(this._field.field).getFormattedValue();
                if (value === null){
                    value = "";
                }
            }

            if (value == this._checkedValue){
                this._inputElement.checked = true;
            } else {
                this._inputElement.checked = false;
            }

            this._onEditChange();

        },

        _updateDataset: function(){

            var newValue = this._inputElement.checked ?
                                    this.getAttribute('checked-value') :
                                    this.getAttribute('unchecked-value');

            var currentRow = this._dataset.getCurrentRow();
            // No current row?
            if (currentRow === null){
                return;
            }
            var fieldObject = currentRow.getFieldObject(this._field.field);

            var oldValue = fieldObject.getValue();

            if (oldValue != newValue){
                console.log('setField', this._field.field, newValue);
                currentRow.setField(this._field.field, newValue);
            }
        },

        render: function(){
            FORM.CheckInput.superclass.render.apply(this, arguments);

            this._inputElement = this.createElement('input', 'text-edit');
            this._inputElement.setAttribute('type', 'checkbox');

            this._container.appendChild(this._inputElement);

            var wc = this.getWidgetContext();
            wc.addManagedDOMEvent(this._inputElement, "onclick", this._onClick, this);

        },


        _onClick: function(e){
            this._updateDataset();
        },

        _onEditChange: function(){
            if (this._isEditable){
                var currentRow = this._dataset.getCurrentRow();

                // No current row or field is read-only
                if (currentRow === null ||
                    currentRow.getFieldObject(this._field.field).getMetadata().isReadOnly()){
                    this._inputElement.disabled = "disabled";
                } else {
                    this._toggleEditableClasses(true);
                    this._inputElement.disabled = false;
                    return;
                }
            } else {
                this._inputElement.disabled = "disabled";
            }

            this._toggleEditableClasses(false);
        },

        getNodeName: function(){
            return 'check-input';
        },

        toString: function(){
            return this.getNodeName();
        }
    });
})();
