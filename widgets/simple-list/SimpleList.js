/*global
YAHOO
*/

(function() {
    YAHOO.namespace("lib_widget_input");
    var lib_widget_input = YAHOO.lib_widget_input;
    var Toronto = YAHOO.com.aviarc.framework.toronto;

    lib_widget_input.SimpleList = function() {
        this.onRowSelected = new Toronto.client.Event("onRowSelected");
        this.onClick = new Toronto.client.Event("onClick");
        this.onKeyDown = new Toronto.client.Event("onKeyDown");
        this.onKeyUp = new Toronto.client.Event("onKeyUp");
    };

    YAHOO.lang.extend(lib_widget_input.SimpleList, Toronto.framework.DefaultWidgetImpl, {
        // The 'startup' method may be deleted if it is not required, the method from DefaultWidgetImpl will be used
        // Removing the superclass.startup method call may prevent your widget from functioning
        startup: function(widgetContext) {
            lib_widget_input.SimpleList.superclass.startup.apply(this, arguments);

            this._showValue = this.getAttribute('show-value');
            this._showField = this.getAttribute('show-field');
            this._field = this.getAttribute('field');
            this._name = this.getAttribute('name');

            this._container = this.getNamedElement("div");

            widgetContext.addManagedDOMEvent(this._container, "onkeydown", this._onKeyDown, this);
            
            this._rows = new Map();

        },

        // The 'bind' method may be deleted if it is not required, the method from DefaultWidgetImpl will be used
        // Removing the superclass.bind method call may prevent your widget from functioning
        bind: function(dataContext) {
            lib_widget_input.SimpleList.superclass.bind.apply(this, arguments);

            this._dataset = dataContext.findDataset(this.getAttribute('dataset'));

            this._dataset.onContentsReplaced.bindHandler(this.refresh, this);

            this._dataset.onCurrentRowChanged.bindHandler(this._currentRowChanged, this);

            this._ulnode = document.createElement('ul');
            this._ulnodeId = this._name + '-ul-node';
            this._ulnode.setAttribute('id', this._ulnodeId);

            this._container.appendChild(this._ulnode);

            this._currentlyFocusedRowIndex = this._dataset.getCurrentRowIndex();
        },

        refresh: function() {
            this._currentlyFocusedRowIndex = this._dataset.getCurrentRowIndex();

            lib_widget_input.SimpleList.superclass.refresh.apply(this, arguments);

            var rowcount = this._dataset.getRowCount();

            this._ulnode.innerHTML = '';
            this._rows.clear();

            this._dataset.getAllRows().doLoop(this._newRow, this);
        },

        focus: function() {
            var currowindex = this._dataset.getCurrentRowIndex();

            if (currowindex == -1) {
                this._dataset.reset();

                currowindex = this._dataset.getCurrentRowIndex();
            }

            if (currowindex != -1) {
                var arow = this._dataset.setCurrentRowIndex(currowindex);

                var arowid = arow.getRowID();
                var ele = document.getElementById(arowid);

                ele.focus();
            }
        },

        _newRow: function(dsRow) {
            var row = new lib_widget_input.SimpleList.Row(dsRow, this);

            this._rows.put(dsRow.getRowID(), row);

            row.render();
        },

        _currentRowChanged: function() {
            this.focus();
        },

        _onKeyDown: function(e) {
            if (e.keyCode == 40 || e.keyCode == 38) {

                currowindex = this._dataset.getCurrentRowIndex();
                rowcount = this._dataset.getRowCount()
                found = false;

                if (e.keyCode == 40) {
                    for (i = currowindex + 1; i < rowcount; i++) {
                        if (this._isRowIndexShown(i)) {
                            found = true;
                            currowindex = i;
                            break;
                        }
                    }
                }

                if (e.keyCode == 38) {
                    for (i = currowindex - 1; i >= 0; i--) {
                        if (this._isRowIndexShown(i)) {
                            found = true;
                            currowindex = i;
                            break;
                        }
                    }
                }

                this._dataset.setCurrentRowIndex(currowindex);
            } else {
                if (e.keyCode !== 13) {
                    this.onKeyDown.fireEvent({});
                }
            }
        },

        _isRowIndexShown: function(index) {
            arow = this._dataset.getRow(index);
            arowid = arow.getRowID();
            anitem = this._rows.get(arowid);

            return anitem._isShown;
        },

        _getNextRow: function() {
            arow = this._dataset.nextRow();

            if (arow !== null) {
                anitem = this._rows.get(arow.getRowID());

                if (!anitem._isShown) {
                    this._getNextRow();
                }
            }
        }


    });


    lib_widget_input.SimpleList.Row = function(datasetRow, simpleList) {
        this._datasetRow = datasetRow;
        this._simpleList = simpleList;
        this._showFieldValue = datasetRow.getField(this._simpleList._showField);
        this._listItem = document.createElement('li')
        this._listItemLink = document.createElement('a');
        this._isShown = false;

    };

    lib_widget_input.SimpleList.Row.prototype = {

        getDatasetRow: function() {
            return this._datasetRow;
        },

        render: function() {
            var fieldvalue = this._datasetRow.getField(this._simpleList._field);
            var fieldid = this._datasetRow.getRowID();

            this._listItem.innerHTML = '';

            this._listItemLink.setAttribute('href', '#');
            this._listItemLink.setAttribute('id', fieldid);
            this._listItemLink.appendChild(document.createTextNode(fieldvalue));

            this._listItem.appendChild(this._listItemLink);
            this._simpleList._ulnode.appendChild(this._listItem);

            this._datasetRow.bindOnFieldChangedHandler(this._simpleList._showField, this._showFieldChanged, this);

            this._clickEvent = this._simpleList.getWidgetContext().createManagedEvent();
            this._listItem.onclick = this._clickEvent.makeDOMEventFunction();
            this._clickEvent.bindHandler(this._liClicked, this);

            this._focusListItemEvent = this._simpleList.getWidgetContext().createManagedEvent();
            this._listItem.onfocus = this._focusListItemEvent.makeDOMEventFunction();
            this._focusListItemEvent.bindHandler(this._liFocus, this);

            this._focusListItemLinkEvent = this._simpleList.getWidgetContext().createManagedEvent();
            this._listItemLink.onfocus = this._focusListItemLinkEvent.makeDOMEventFunction();
            this._focusListItemLinkEvent.bindHandler(this._liLinkFocus, this);

            //if I am deleted, or my show field is set to hide me, don't show me.

            if (this._datasetRow.isMarkedDeleted()) {
                this._hideme();
            } else {
                this._showFieldChanged();
            }
        },

        _showFieldChanged: function() {

            if (this._datasetRow.getField(this._simpleList._showField) === this._simpleList._showValue) {
                this._showme();
            } else {
                this._hideme();
            }
        },

        _liClicked: function() {
            this._simpleList.onClick.fireEvent({
                widget: this._simpleList.getWidgetContext().getWidgetNode()
            });
        },

        _makeMeSelected: function() {
            this._datasetRow.makeCurrentRow();

            this._simpleList.onRowSelected.fireEvent({
                widget: this._simpleList.getWidgetContext().getWidgetNode(),
                datasetRow: this._datasetRow
            });
        },

        _hideme: function() {
            this._listItem.style.display = "none";
            this._isShown = false;
        },

        _showme: function() {
            this._listItem.style.display = "block";
            this._isShown = true;
        },

        _liFocus: function() {
            this._makeMeSelected();
        },

        _liLinkFocus: function() {
            this._makeMeSelected();
        }
    };



})();