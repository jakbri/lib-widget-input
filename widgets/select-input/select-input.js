(function () {

    YAHOO.namespace("lib_widget_input");
    var FORM = YAHOO.lib_widget_input;

    // Javascript key codes
    var KEY = {
        ENTER: 13,
        SPACE: 32,
        UP: 38,
        DOWN: 40,
        ESC: 27
    };

    var L = YAHOO.lang;
    var D  = YAHOO.util.Dom;

    FORM.SelectInput = function(){};

    L.extend(FORM.SelectInput, FORM.DefaultFormWidget, {

        startup: function(widgetContext) {
            FORM.SelectInput.superclass.startup.apply(this, arguments);

            var field = this.getAttribute('field').split('.');
            this._field = {'dataset': field[0], 'field': field[1]};

            this._idField = this.getAttribute('id-field');
            this._displayField = this.getAttribute('display-field');

            this._selectionDisplayField = this.getAttribute('selection-display-field');
            if (this._selectionDisplayField === "" ){
                this._selectionDisplayField = null;
            } else {
                var split = this._selectionDisplayField.split('.');
                this._selectionDisplayField = {'dataset': split[0], 'field': split[1] };
                var ignoreWidget = widgetContext.findWidget('mark-rows-updated');
                if (ignoreWidget !== null && ignoreWidget !== undefined){
                    ignoreWidget.ignoreField(split[0], split[1]);
                }
            }

            this._nullOption = this.getAttribute('null-option-text');

            this._multiSelect = parseBoolean(this.getAttribute('multi-select'));
            if (this._multiSelect){
                D.addClass(this._container, 'multi-select');
            }

            this._isShown = false;

            this._dropEvents = [];
        },

        bind: function(dataContext) {
            FORM.SelectInput.superclass.bind.apply(this, arguments);

            this._dataset = dataContext.findDataset(this._field.dataset);

            this._dataset.onCurrentRowChanged.bindHandler(this._updateInput, this);
            this._dataset.onContentsReplaced.bindHandler(this._updateInput, this);
            this._dataset.bindOnCurrentRowFieldChangedHandler(this._field.field, this._updateInput, this);

            this.getWidgetContext().getWidgetNode().onHide.bindHandler(this._hidePopup, this);

            this._valuesDataset = dataContext.findDataset(this.getAttribute('dataset'));
            this._valuesDataset.onRowFieldChanged.bindHandler( this._updateOptions, this);
            this._valuesDataset.onContentsReplaced.bindHandler( this._updateOptions, this);
        },

        refresh: function() {
            FORM.SelectInput.superclass.refresh.apply(this, arguments);

            this._updateOptions();
            this._updateInput();
        },

        _updateOptions: function(){
            if (this._isRendered === false){
                return;
            }

            var rows = this._valuesDataset.getAllRows();
            var wc = this.getWidgetContext();
            var currentRow = this._dataset.getCurrentRow();
            var currentValue = null;

            this._innerDropDown.innerHTML = '';

            for (var e; e < this._dropEvents.length; e ++){
                this._dropEvents[e].unbind();
            }
            this._dropEvents = [];

            if (currentRow !== null){
                currentValue = currentRow.getField(this._field.field);
            }
            var li, option, widgetEvent;
            if(this._nullOption !== undefined){
                li = document.createElement('li');
                li.setAttribute('rel', -1);
                option = this._makeOption("",this._nullOption , "");
                li.appendChild(option);
                D.addClass(li, 'null-option-text');

                this._innerDropDown.appendChild(li);
                widgetEvent = wc.addManagedDOMEvent(option, 'onclick', this._onLiClick, this);
                this._dropEvents.push(widgetEvent);
            }

            for (var i =0 ; i < rows.length; i ++ ) {
                var row = rows[i];
                if (row.isMarkedDeleted()){
                    continue;
                }

                var idValue = row.getField(this._idField);
                var displayValue = row.getFieldObject(this._displayField).getFormattedValue();

                li = document.createElement('li');
                li.setAttribute('rel', i);
                // var option = this._makeOption(idValue, displayValue, row.getRowID());
                option = this._makeOption(idValue, displayValue, row.getRowID());
                li.appendChild(option);
                // this._selectList.appendChild(option);
                this._innerDropDown.appendChild(li);
                widgetEvent = wc.addManagedDOMEvent(option, 'onclick', this._onLiClick, this);
                this._dropEvents.push(widgetEvent);
            }
        },

        _makeOption: function(value, text, rowID){
            var option = document.createElement('a');
            option.setAttribute('tabindex', '0');

            var glyph = document.createElement('i');
            glyph.setAttribute('class', 'glyph');

            var span = document.createElement('span');
            span.setAttribute('class', 'text');
            if (text === '' || text === null){
                text = '&nbsp;';
            }
            span.innerHTML = text;



            option.setAttribute('rowID', rowID);
            option.setAttribute('value', value);

            option.appendChild(glyph);
            option.appendChild(span);

            return option;
        },


        _updateInput: function(){
            if (this._isRendered === false){
                return;
            }

            var currentRow = this._dataset.getCurrentRow();
            var value = "";
            if (currentRow !== null){
                var fieldObject = currentRow.getFieldObject(this._field.field);
                value = fieldObject.getFormattedValue();
                this.bindMetadata(fieldObject);
                if (value === null){
                    value = "";
                }
            }

            var output = "";
            var rows = this._valuesDataset.getAllRows();
            var values = value.split(',');
            for (var i =0 ; i < rows.length; i ++){
                var rowValue = rows[i].getField(this._idField);
                if (this._multiSelect){
                    if (values.indexOf(rowValue) > -1){
                        output += rows[i].getFieldObject(this._displayField).getFormattedValue() + ', ';
                    }
                }else {
                    if (rowValue == value){
                        output = rows[i].getFieldObject(this._displayField).getFormattedValue();
                        break;
                    }
                }
            }
            if (this._multiSelect && output.length > 0){
                output = output.substring(0, output.length - 2);
            }

            if (output === ""  && this._nullOption !== undefined){
                output = this._nullOption;
                D.addClass(this._textElement, 'null-option');
            } else {
                D.removeClass(this._textElement, 'null-option');
            }

            this._textElement.innerHTML = output;

            if (this._selectionDisplayField !== null){
                if (currentRow !== null){
                    currentRow.setField(this._selectionDisplayField.field, output);
                }
            }

            this._onEditChange();
            this._updateSelected(value);
        },

        _updateSelected: function(value){
            var options = this._innerDropDown.children;
            var values = value.split(",");

            for (var i = 0; i < options.length; i ++){

                var li = options[i];

                var a = li.firstChild;
                var found = false;

                if (this._multiSelect){
                    var index = values.indexOf(a.getAttribute('value'));
                    if (index > -1){
                        found = true;
                    }

                } else {
                    if (a.getAttribute('value') == value){
                        found = true;
                    }
                }

                if (found){
                    D.addClass(li, 'selected');
                } else {
                    D.removeClass(li, 'selected');
                }
            }
        },

        render: function(){
            FORM.SelectInput.superclass.render.apply(this, arguments);

            this._inputElement = this.createElement('button');
            this._inputElement.setAttribute('class', 'btn select-toggle');

            this._textElement = this.createElement('div');
            this._textElement.setAttribute('class', 'select-text');

            this._inputElement.appendChild(this._textElement);


            this._dropDownElement = this.createElement('div');
            this._dropDownElement.setAttribute('class', 'dropdown-menu display-none');

            this._innerDropDown = this.createElement('ul');
            this._innerDropDown.setAttribute('class', 'dropdown-menu inner');
            this._dropDownElement.appendChild(this._innerDropDown);

            this._container.appendChild(this._inputElement);
            this._container.appendChild(this._dropDownElement);

            var wc = this.getWidgetContext();
            wc.addManagedDOMEvent(this._inputElement, "onclick", this._onButtonClick, this);

            var managedOnClickEvent = wc.createManagedEvent("document:onclick");
            var managedOnKeyDown = wc.createManagedEvent("document:onkeydown");

            YAHOO.util.Event.addListener(document, 'click', managedOnClickEvent.makeDOMEventFunction(), this, true);
            YAHOO.util.Event.addListener(document, 'keydown', managedOnKeyDown.makeDOMEventFunction(), this, true);

            managedOnKeyDown.bindHandler(this._onKeyDown, this);
            managedOnClickEvent.bindHandler(this._onDocumentClick, this);
        },

        _findSelect: function(element, maxCount){
            var count = 0;

            while ( element !== undefined  && element !== null){
                //console.log(count, element, this._container);
                if (element == this._dropDownElement || element == this._container){
                    return count;
                }

                element = element.parentElement;
                count ++;
                if (count > maxCount){
                    return -1;
                }
            }

            return -1;
        },

        _onDocumentClick: function(e){
            if (this._findSelect(e.target, 5) == -1){
                this._hidePopup();
            }
        },

        _hidePopup : function() {
            // Put context menu popup back where it was before we put it at the end of the HTML doc
            this._container.appendChild(this._dropDownElement);
            D.addClass(this._dropDownElement, 'display-none');
            this._isShown = false;
        },

        _showPopup : function () {
            // Move context popup to end of HTML document so that it is above everything else
            document.body.appendChild(this._dropDownElement);
            D.removeClass(this._dropDownElement, 'display-none');

            var coords = new selectInputEncloseElementWithinScreen().evaluatePosition(this._dropDownElement, this._inputElement);
            if (coords.nearestScrollableContainer !== null) {
                coords.nearestScrollableContainer.appendChild(this._dropDownElement);
            }
            coords.y = parseInt(coords.y, 10) + 25; // Account for height of button

            this._dropDownElement.style.left = coords.x + "px";
            this._dropDownElement.style.top  = coords.y + "px";

            this._isShown = true;
        },


        _onButtonClick: function(e){
            //console.log ('buttonClick');
            if (this._isShown){
                this._hidePopup();
                return;
            }
            this._setDropDownSize();

            this._showPopup();
        },

        _onLiClick: function(e){

            var target = e.target;

            // find the A
            while (target !== undefined){
                if (target.nodeName == 'A'){
                    break;
                }

                target = target.parentNode;
            }

            if (target === undefined){
                return;
            }

            var rowID = target.getAttribute('rowID'),
                value = target.getAttribute('value'),
                selectedNullOption = this._nullOption && value === '';

            if (this._multiSelect) {
                var values = this._dataset.getCurrentRow().getField(this._field.field);

                if (values === null || values === "") {
                    values = [];
                } else {
                    values = values.split(',');
                }


                if (D.hasClass(target.parentNode, 'selected') && !selectedNullOption) {
                    // uncheck
                    D.removeClass(target.parentNode, 'selected');
                    var index = values.indexOf(value);
                    values.splice(index, 1);
                } else {
                    // check
                    D.addClass(target.parentNode, 'selected');
                    values.push(value);
                }

                if(selectedNullOption) {
                    value = '';
                } else {
                    value = values.join(',');
                }

                this._dataset.getCurrentRow().setField(this._field.field, value);
                return;
            }

            this._dataset.getCurrentRow().setField(this._field.field, value);

            var children = this._innerDropDown.children;
            for (var i = 0; i < children.length; i++){
                D.removeClass(children[i], 'selected');
            }

            D.addClass(target.parentNode, 'selected');
            this._hidePopup();

        },

        _setDropDownSize : function(){

            var left = this._inputElement.offsetLeft;
            var top = this._inputElement.offsetTop + this._inputElement.clientHeight + 3;
            var width = this._inputElement.clientWidth;
            var height = (this._innerDropDown.children.length * 26);
            var windowHeight = window.innerHeight;
            var windowHeightFactor = (windowHeight > 800) ? 3.0 : 2.0;    // iPad factor
            var maxDropDownHeight = Math.floor(window.innerHeight * 0.2 * windowHeightFactor);

            if (height > maxDropDownHeight) {
                height = maxDropDownHeight;
                this._dropDownElement.style.overflowY = 'scroll';
            }

            this._dropDownElement.style.top = top + 'px';
            this._dropDownElement.style.left = left + 'px';
            this._dropDownElement.style.minWidth = (width + 4) + 'px';
            this._dropDownElement.style.height = height + 'px';
            this._dropDownElement.style.position = 'absolute';
        },

        _getFocused: function(){

            var focusedElement = document.activeElement;
            var items = this._innerDropDown.children;

            for (var i = 0; i < items.length; i ++){
                if (items[i].firstChild == focusedElement){
                    return i;
                }
            }

            return -1;
        },

        _onKeyDown: function(e){
            // not called on our select-list
            if (this._findSelect(e.target, 5) === -1){
                return;
            }

            var focused = 0;
            // Cycle through the selections when Up or Down is pressed
            if ( (KEY.UP == e.keyCode ||  KEY.DOWN == e.keyCode ) &&
                    this._isShown){
                var itemLength = this._innerDropDown.children.length;
                focused = this._getFocused();

                if (e.keyCode == KEY.DOWN){
                    focused ++;
                    if (focused >= itemLength){
                        focused = 0;
                    }
                } else {
                    focused -- ;
                    if (focused < 0){
                        focused = itemLength - 1;
                    }
                }

                this._innerDropDown.children[focused].firstChild.focus();

                e.preventDefault();
                return;
            }

            // Select a object if Space or Enter was pressed
            if (  (KEY.SPACE == e.keyCode || KEY.ENTER == e.keyCode) && this._isShown ){
                focused = this._getFocused();

                if (focused === -1){
                    return;
                }

                e.preventDefault();
                this._innerDropDown.children[focused].firstChild.click();
                return;
            }

            // Open the select list
            if ( [KEY.ENTER, KEY.SPACE, KEY.UP, KEY.DOWN].indexOf(e.keyCode) > -1 &&
                    !this._isShown){
                e.preventDefault();
                this._onButtonClick();
                return;
            }

            if (e.keyCode == KEY.ESC && this._isShown){
                this._hidePopup();
                e.preventDefault();
                return;
            }

        },

        _onEditChange: function(){
            if (this._isEditable){
                var currentRow = this._dataset.getCurrentRow();

                // No current row or field is read-only
                if (currentRow === null ||
                    currentRow.getFieldObject(this._field.field).getMetadata().isReadOnly()){
                    this._inputElement.disabled = "disabled";
                } else {
                    this._toggleEditableClasses(true);
                    this._inputElement.disabled = false;
                    return;
                }
            } else {
                this._inputElement.disabled = "disabled";
            }

            this._toggleEditableClasses(false);
        },

        getNodeName: function(){
            return 'select-input';
        },

        toString: function(){
            return this.getNodeName();
        }
    });



    /**
    * Calculates the size and location of the elementToPosition and returns the new coordinates within the screen,
    * making shure that the elementToPosition is attached to the referenceElement by it's corner.
    * @method: selectInputEncloseElementWithinScreen
    * @param elementToPosition - The element to be positioned within the screen.
    * @param referenceElement: The element which the elementToPosition is positioned on.
    * @return: coordinates (x,y)
    */
    function selectInputEncloseElementWithinScreen(){

    }

    selectInputEncloseElementWithinScreen.prototype = {

        evaluatePosition: function(elementToPosition, referenceElement) {
            /**
            * This object returns the Dimensions of the ElementToPosition:
            */
            var elementToMoveDimensions = {
                "width" : elementToPosition.clientWidth + elementToPosition.style.padding,
                "height" : elementToPosition.clientHeight + elementToPosition.style.padding
            };
            // This object returns the Dimensions of the referenceElement:
            var referenceElementDimensions = {
                "width": referenceElement.clientWidth,
                "height": referenceElement.clientHeight
            };

            var position = this._findElementPosition(referenceElement, elementToMoveDimensions.height);
            var coord = this._findCorner(position, elementToMoveDimensions); // find the bottom right corner of elementToMove
            var refCoord = this._findCorner(position, referenceElement); // find the bottom right corner of referenceElement

            var screenWidth = YAHOO.util.Dom.getViewportWidth();
            var screenHeight = YAHOO.util.Dom.getViewportHeight();

            /**
            * This is the logic for determining where the elementToPosition is to be located.
            * @param: referenceElementDimensions, elementToMoveDimensions
            * @return: destHoz, destVert
            */
            var destHoz = 0;
            if (screenWidth < coord.rightCoord) {        //if the right side of the element is on or outside the right edge of the screen.
                var distFromRight = parseInt(screenWidth, 10) - parseInt(refCoord.rightCoord, 10);
                destHoz = parseInt(refCoord.rightCoord, 10) - parseInt(elementToMoveDimensions.width, 10);            // reflect to the left
                // destHoz += referenceElementDimensions.width / 2;
                // destHoz -= parseInt(referenceElementDimensions.width, 10);
            } else {
                destHoz = position.leftOffset; // + referenceElementDimensions.width / 2;
                // if the element does not appear outside of the screen leave right aligned.
            }
            var destVert = 0;
            if (screenHeight < coord.bottomCoord) { //if the right side of the element is on or outside the edge of the screen.
                destVert = position.topOffset;        // reflect to the top
                // destVert += referenceElementDimensions.height / 2;
                // destVert -= parseInt(elementToMoveDimensions.height, 10);
            }
            else {
                destVert = position.topOffset; // + referenceElementDimensions.height / 2;
            }
            /** The following returns the coordnates of the elements top left corner as "x" and "y".
            * @return: "x" = horizontal destination for the element.
            * @return: "y" = vertical destination for the element.
            */
            return {
                x : destHoz,
                y : destVert,
                nearestScrollableContainer : position.nearestScrollableContainer
            };
        },


        /**
        *    This function finds the postion of the element and returns its offset coordinates,
        *    as well as the total offset coordinates of all its parent elements
        *     @param: elementToLocate
        *     @param: height
        *     @return: leftOffset - position of the reference element on the horizontial axis
        *     @return: topOffset - position of the reference element on the vertical axis
        */
        _findElementPosition : function (elementToLocate, height) {
            var locLeft = 0,  // The left location of the box
                locTop = 0,   // The top location of the box
                nearestScrollableContainer = null;
            // Total of the offset locations of the box and all its parent locations to arrive with the actual screen location of the box.
            if (elementToLocate.offsetParent) {        // If the element has an offsetparent calculate the offset and add it to the locLeft/locTop total until there are no more offset parents.
                do {
                    if (nearestScrollableContainer === null
                        && (elementToLocate.scrollHeight - (height + 20)) > elementToLocate.offsetHeight
                        && elementToLocate.offsetHeight != 0) {

                        nearestScrollableContainer = elementToLocate;
                        break;
                    }

                    locLeft += elementToLocate.offsetLeft - elementToLocate.scrollLeft;
                    locTop += elementToLocate.offsetTop - elementToLocate.scrollTop;

                    elementToLocate = elementToLocate.offsetParent;
                } while (elementToLocate.offsetParent !== null);
                // While the box has a parent sum up the total offset of the box plus it's parent elements, to find the location of the box.
            }

            return {
                nearestScrollableContainer: nearestScrollableContainer,
                leftOffset : locLeft,
                topOffset  : locTop
            };
        },

        /**function for finding the bottom right corner of the box.
        * @param: elementToMoveDimensions, postion
        * @return: coord.bottomCoord, coord.rightCoord
        */

        _findCorner : function (position, elementToMoveDimensions) {

            var rightCoord = parseInt(position.leftOffset, 10) + parseInt(elementToMoveDimensions.width || elementToMoveDimensions.clientWidth, 10);
            var bottomCoord = parseInt(position.topOffset, 10) + parseInt(elementToMoveDimensions.height ||  elementToMoveDimensions.clientHeight, 10);

            return { "bottomCoord" : bottomCoord,
                      "rightCoord" : rightCoord};
        }
    };
})();
