(function () {

    YAHOO.namespace("lib_widget_input");
    var FORM = YAHOO.lib_widget_input;

    var L = YAHOO.lang;
    var D  = YAHOO.util.Dom;

    FORM.TextInputExtend = function(){};

    L.extend(FORM.TextInputExtend, FORM.DefaultFormWidget, {

        startup: function(widgetContext) {
            FORM.TextInputExtend.superclass.startup.apply(this, arguments);

            this._timeoutCounter = 0;
            this._writeToDatasetFieldOnTimeout = parseBoolean(this.getAttribute('write-to-dataset-field-on-timeout'));
            this._writeToDatasetFieldTimeout = parseBoolean(this.getAttribute('write-to-dataset-field-timeout'));

            var field = this.getAttribute('field').split('.');
            this._field = {'dataset': field[0], 'field': field[1]};

            var type = this.getAttribute('type');
            if (type == 'date'){
                this._showDatePicker = true;
            }

            this._datePicker = null;
            this._datePickerName = null;
            if (this.getAttribute('date-picker-name') === "") {
                this._datePickerName = this._field.dataset + '-' + this._field.field + '-date-picker';
            }
            else {
                this._datePickerName = this.getAttribute('date-picker-name');
            }


            this._multiLine = parseBoolean(this.getAttribute('multi-line'));
            if (this._multiLine){
                D.addClass(this._container, "multi-line");
            }

            //extender properties
            this._extended = false;

            //original height
            this._originalTextInputHeight = 100;

            //multiplier
            this._multiplicationFactor = 2;
        },

        _extenderClicked: function() {

            //based on this.extended either extend or contract the record table.
            if(!this._extended) {
                this._extended = true;
                this._heightExtenderLabel.innerHTML = "&#9650; I want to see less";
                this.getContainerElement().style.height = (this._multiplicationFactor * this._originalTextInputHeight)+'px';
            }
            else {
                this._extended = false;
                 this._heightExtenderLabel.innerHTML = "&#9660; I want to see more" ;
                this.getContainerElement().style.height = this._originalTextInputHeight + 'px';
            }

        },

        bind: function(dataContext) {
            FORM.TextInputExtend.superclass.bind.apply(this, arguments);

            this._dataset = dataContext.findDataset(this._field.dataset);

            var events = this._dataset.getEvents();
            this._dataset.bindOnCurrentRowFieldChangedHandler(this._field.field, this._updateInput, this);
            this._dataset.onContentsReplaced.bindHandler( this._updateInput, this);
            this._dataset.onCurrentRowChanged.bindHandler( this._updateInput, this);
        },

        refresh: function() {
            FORM.TextInputExtend.superclass.refresh.apply(this, arguments);

            this._updateInput();
        },

        _updateInput: function(){
            if (this._isRendered === false){
                return;
            }

            var currentRow = this._dataset.getCurrentRow();
            var value = "";
            if (currentRow !== null){
                var fieldObject = currentRow.getFieldObject(this._field.field);
                value = fieldObject.getFormattedValue();
                this.bindMetadata(fieldObject);
                if (value === null){
                    value = "";
                }
            }


            this._inputElement.value = value;
            this._showElement.innerHTML = value;

            this._onEditChange();
        },

        _updateDataset: function(){
            var newValue = this._inputElement.value;

            var currentRow = this._dataset.getCurrentRow();
            // No current row?
            if (currentRow === null){
                this.bindMetadata(null);
                return;
            }
            var fieldObject = currentRow.getFieldObject(this._field.field);

            var oldValue = fieldObject.getFormattedValue();

            if (oldValue != newValue){
                fieldObject.setFormattedValue(newValue);
            }

        },

        render: function(){
            FORM.TextInputExtend.superclass.render.apply(this, arguments);

            if (this._multiLine){
                this._inputElement = this.createElement('textarea', 'text-edit');
            } else {
                this._inputElement = this.createElement('input', 'text-edit');
            }

            /* this._inputElement = this.createElement('textarea', 'text-edit'); */
            this._showElement = this.createElement('div', 'text-edit');

            this._container.appendChild(this._inputElement);
            this._container.appendChild(this._showElement);

            this._seeMore();

            var wc = this.getWidgetContext();
            wc.addManagedDOMEvent(this._inputElement, "onblur", this._onBlur, this);
            wc.addManagedDOMEvent(this._inputElement, "onfocus", this._onFocus, this);
            wc.addManagedDOMEvent(this._inputElement, "onkeydown", this._onKeyDown, this);
            wc.addManagedDOMEvent(this._inputElement, "onkeyup", this._onKeyUp, this);

            if (this._showDatePicker){
                this._datePicker = this.getWidgetContext().findWidget(this._datePickerName);
                if (this._datePicker === null || this._datePicker === undefined ){
                    console.log('Unable to find date-picker called:' + this._datePickerName );
                }
            }

        },

        _onKeyDown: function(e){
            if (!this._isMultiLine && e.keyCode == 13) {
                this._updateDataset();
            }
        },

        _onKeyUp: function(e){

            if (this._writeToDatasetFieldOnTimeout) {

                this._timeoutCounter++;
                var me = this;
                var tempCount = me._timeoutCounter;
                window.setTimeout(
                            "Toronto.client.GlobalState.getWidgetTree().getWidgetByID('"+this.getAttribute("name")+"').getWidget().getWidget()._timeOutFunc("+tempCount+");"
                            ,this.getAttribute("write-to-dataset-field-timeout")
                );
            }
        },

        _timeOutFunc: function(tempCount){
            var me = this;

            if (tempCount == me._timeoutCounter){
                this._updateDataset();
            }
        },

        _onFocus: function(e){
        },

        _onBlur: function(e){
            this._updateDataset();
        },

        _onEditChange: function(){
            if (this._isEditable){
                var currentRow = this._dataset.getCurrentRow();

                // No current row or field is read-only
                if (currentRow === null ||
                    currentRow.getFieldObject(this._field.field).getMetadata().isReadOnly()){
                    //this._inputElement.disabled = "disabled";
                    D.addClass(this._inputElement, "display-none");
                    D.removeClass(this._showElement, "display-none");
                    this.setDatePickerVisible( false );
                } else {
                    this._toggleEditableClasses(true);
                    //this._inputElement.disabled = false;
                    D.removeClass(this._inputElement, "display-none");
                    D.addClass(this._showElement, "display-none");
                    this.setDatePickerVisible( true );
                    return;
                }
            } else {
                //this._inputElement.disabled = "disabled";
                D.addClass(this._inputElement, "display-none");
                D.removeClass(this._showElement, "display-none");
                this.setDatePickerVisible( false );
            }

            this._toggleEditableClasses(false);
        },

        _seeMore: function(){
            // This function creates 'I want to see more' thingy

            // Uncomment these lines if Roz comes back and asks to keep those links only
            // when there is long texts
            // var currentRow = this._dataset.getCurrentRow();
            // if(this._multiLine && currentRow
            //                     && currentRow.getFieldObject(this._field.field)
            //                     && currentRow.getFieldObject(this._field.field).getFormattedValue() !== "") {
            if(this._multiLine) {
                this._heightExtender = this.createElement('div', 'extendheightdiv');
                this._heightExtender.setAttribute('class', 'extend-height');
                this._heightExtenderLabel = this.createElement('div', 'extenderheightlabel');
                this._heightExtenderLabel.innerHTML = "&#9660; I want to see more";
                this._heightExtenderLabel.setAttribute('class', 'extender-height-label');
                this._heightExtender.appendChild(this._heightExtenderLabel);

                this._container.appendChild(this._heightExtender);
                this._heightClickEvent = this.getWidgetContext().createManagedEvent();
                this._heightExtender.onclick = this._heightClickEvent.makeDOMEventFunction();
                this._heightClickEvent.bindHandler(this._extenderClicked, this);
            }
        },

        setDatePickerVisible: function( visible ) {
            if (this._datePicker !== null && this._datePicker !== undefined ){
                var container = this._datePicker.getContainerElement();
                if (visible){
                     D.removeClass(container, "display-none");
                } else {
                     D.addClass(container, "display-none");
                }
                this._datePicker.setEnabled(visible);
            }
        },

        getNodeName: function(){
            return 'text-input-extend';
        },
        toString: function(){
            return this.getNodeName();
        }
    });
})();
