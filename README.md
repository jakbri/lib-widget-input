Input
====



```
#!xml

This library provides Aviarc input widgets, widgets that allow a user to change data in a dataset through the UI

```

## Environment ##
This application uses Maven as its build and dependency management tool.
The build ```environment will need``` access to wsd-nexus.

The target Aviarc version is 3.6.0+ using the Toronto widget set.

There is no target servlet container.

There is no target java version (although java < version 7 is unsupported).

There is no target OS.

## Maintainers ##

* Jacob Briggs