package com.aviarc.form;

import org.w3c.dom.Element;
import org.w3c.dom.Node;

import com.aviarc.core.components.AviarcURN;
import com.aviarc.framework.toronto.screen.CompiledWidget;
import com.aviarc.framework.toronto.screen.RenderedNode;
import com.aviarc.framework.toronto.screen.ScreenRenderingContext;
import com.aviarc.framework.toronto.widget.DefaultDefinitionFile;
import com.aviarc.framework.toronto.widget.DefaultRenderedNodeFactory;
import com.aviarc.framework.toronto.widget.DefaultRenderedWidgetImpl;
import com.aviarc.framework.xml.compilation.ResolvedElementContext;

public class DefaultFormWidget implements DefaultRenderedNodeFactory {

    private static final long serialVersionUID = 1L;
    private DefaultDefinitionFile _definition;

    /*
     * (non-Javadoc)
     * @see com.aviarc.framework.toronto.widget.DefaultRenderedNodeFactory#initialize(com.aviarc.framework.toronto.widget.DefaultDefinitionFile)
     */
    public void initialize(DefaultDefinitionFile definitionFile) {
        // Store the definition - our rendered node class requires it as it derives from DefaultRenderedNodeImpl
        this._definition = definitionFile;
    }

    /*
     * (non-Javadoc)
     * @see com.aviarc.framework.toronto.widget.RenderedNodeFactory#createRenderedNode(com.aviarc.framework.xml.compilation.ResolvedElementContext, com.aviarc.framework.toronto.screen.ScreenRenderingContext)
     */
    public RenderedNode createRenderedNode(ResolvedElementContext<CompiledWidget> elementContext,
                                           ScreenRenderingContext renderingContext) {
        return new DefaultFormWidgetImpl(elementContext, renderingContext, _definition);
    }

    /**
     * Our custom implementation of RenderedNode.
     *
     * It derives from DefaultRenderedNodeImpl so that all the normal behaviour for widgets, e.g. javascript
     * constructors, requirements registering, required datasets etc are taken from the definition file.
     *
     * We override the HTML generation method to provide our own markup.
     *
     */
    public static class DefaultFormWidgetImpl extends DefaultRenderedWidgetImpl {
        public DefaultFormWidgetImpl(ResolvedElementContext<CompiledWidget> resolvedContext,
                                            ScreenRenderingContext renderingContext,
                                            DefaultDefinitionFile definition) {

            super(resolvedContext, renderingContext, definition);
        }

        /**
         * Overridden to generate custom markup.
         */
        @Override
        public Node createXHTML(XHTMLCreationContext context) {
            DefaultDefinitionFile definition = getDefinitionFile();

            // Use the current document to create an element
            Element div = context.getCurrentDocument().createElement("div");
            div.setAttribute("id", String.format("%1$s:container", getAttributeValue("name")));
            div.setAttribute("class", String.format("%1$s_%2$s", makeURNToken(definition.getWidgetURN()), definition.getWidgetName() ));

            return div;
        }

        private String makeURNToken(AviarcURN urn){
            String namespace = urn.getName().replace('.', '-');
            String version = urn.getVersion().toString().replace('.', '-');
            version = version.length() > 0 ? version : null;
            StringBuilder output = new StringBuilder();
            output.append(namespace);
            if (version != null) {
                output.append('_');
                output.append(version);
            }
            return output.toString();
        }
    }

}
